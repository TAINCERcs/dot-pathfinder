<?php
require_once "dotfinder.class.php";
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dotfinder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>

<body>
    <div class="wrapper">

        <?php
            $points = new pathFinder();
            $points::genPoints(10, 10);
            $points::drawPoints();
            
        ?>

    </div>


    <div class="wrapper">
        <?php 
        $points::getPath();

        ?>
    </div>

</body>
</html>